// FOR NUMBER 1

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:{
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	}
	// talk: function(){
	// 	console.log("Pikachu! I choose you!"),
	// }
}

trainer.talk = function(){
	console.log("Pikachu! I choose you!")
}
console.log(trainer);


console.log("Result of Dot Notation:");
console.log(trainer.name);

console.log("Result of Bracket Notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();



// FOR NUMBER 2

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;	

	this.faint = function(){
		console.log(this.name + " fainted!");
	}

	this.tackle = function(targetPokemon){
		console.log(this.name + " tackled " + targetPokemon.name);

		targetPokemon.health = targetPokemon.health - this.attack;

		console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);

		if (targetPokemon.health <= 0){
				targetPokemon.faint();
		}
	}
}


let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);



// FOR NUMBER 3

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);